var constants = require("./constants");
var mysql      = require('mysql');
module.exports = {	
	init:function(){
		this.shipData = constants.shipDataDefault;
		//console.log(JSON.stringify(constants.DB))
		this.connection = mysql.createConnection(constants.DB);
		this.connection.connect(
			//function (err){throw(err);} BUG
			);
	},
	NEW_PLAYER:function(s){		
		this.connection.query('SELECT * FROM users', this.queryCall);
		this.connection.query('INSERT INTO users SET ?', this.randShipData(), this.queryCall);		
	},
	queryCall:function(err, rows){
		if (err) throw err;		
		global.queryResult.push(rows);				
	},
	randShipData:function(){		
		this.shipData.name = "user"+this.randInt(1,65000);
		this.shipData.x = this.randXY("Width");
		this.shipData.y = this.randXY("Height");
		return this.shipData;
	},
	randXY:function(s){
		var ship="ship"+s;
		var stage ="stage"+s;
		return this.randInt(constants[ship]/2,constants[stage]-constants[ship]/2);
	},
	shipData:{},
	randInt:function(low,high){
		return Math.floor(Math.random()*(high-low)+low);
	},
	connection:{},
	//queryResult:[]
  




}