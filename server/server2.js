var net = require('net');
global.queryResult=[];
var mess = require('./functions');
var constants = require("./constants");
mess.init(); 
var server = net.createServer(function (socket) {
     socket.setEncoding("utf8");
	
     //Send the Cross Domain Policy
     socket.write(writeCrossDomainFile() + constants.messageTerminator);
 
     //Due to old Flash Players, this listens for Flash to request
     //the Cross Domain Policy and then responds
     function on_policy_check(data) {
          socket.removeListener('data', on_policy_check);
          socket.on('data', on_data);
 
          try {
               if(data == '<policy-file-request/>'+constants.messageTerminator) {
                    socket.write(writeCrossDomainFile());
               }
               else {
                    on_data(data);
               }
          }
          catch (ex) {
               console.log(ex);
          }
     }
 
     function on_data(d) {
         // socket.write("Echo: " + data);
		 var data = parseData(d);
		 mess[data.name](data.string);
		 socket.write(JSON.stringify(global.queryResult)+constants.messageTerminator);
		 global.queryResult=[];
     }
 
     socket.on('data', on_policy_check);
 
     socket.on("error", function (exception) {
          socket.end();
     });
 
     socket.on("timeout", function () {
          socket.end();
     });
 
     socket.on("close", function (had_error) {
          socket.end();
     });
});
server.listen(9001, "127.0.0.1"); 
console.log("Server started...\n");
 
function writeCrossDomainFile()
{
     var xml = '<?xml version="1.0"?>\n<!DOCTYPE cross-domain-policy SYSTEM'
     xml += ' "http://www.macromedia.com/xml/dtds/cross-domain-policy.dtd">';
     xml += '\n<cross-domain-policy>\n';
     xml += '<allow-access-from domain="*" to-ports="*"/>\n';
     xml += '</cross-domain-policy>\n';
 
     return xml;
}
/**
* Парсим json в объект 
*/
function parseData(d) {
	console.log('parse string');
    var str = d.toString();
	str = str.substring(0, str.length - 1); // Строка приходит с NULL на конце
    var data = JSON.parse(str);
	return data;
}