function define(name,value){
	Object.defineProperty(exports, name, {
		value:value,
		enumerable:true
	});
}
var stageWidth = 640;
var stageHeight = 480;
var shipWidth = 10;
var shipHeight = 18;
define("stageWidth", stageWidth);
define("stageHeight",stageHeight);
define("shipWidth", shipWidth);
define("shipHeight",shipHeight);
define("messageTerminator","\0");
define("DB",{
		  host     : 'localhost',
		  user     : 'root',
		  password : '',
		  database:'my_db'
		});
define("shipDataDefault", {
		name:"",
		x:stageWidth/2,
		y:stageHeight/2,
		angel:0,
		width:shipWidth,
		height:shipHeight
	});		