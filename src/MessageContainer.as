package  
{
	/**
	 * ...
	 * @author Alexander
	 */
	import Entities.JSONEncoder;
	public class MessageContainer
	{
		/** Message name */
		public var name:String;
 
		/** Message string */
		public var string:String;
		private var _sock:Sock;
		private var _enc:JSONEncoder;
		public function MessageContainer(conn:Sock) {
			_sock = conn;
			
		}
 
		/** Message data - to be filled in by client */
		//public var m_data:*;
		
		public function send() {
			_enc = new JSONEncoder(this);
			
			_sock.send(_enc.getString());
			
		}
		
	}

}