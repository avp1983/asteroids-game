package  
{
	/**
	 * ...
	 * @author q
	 */
	import flash.text.TextField;
    import flash.text.TextFieldAutoSize;
    import flash.text.TextFormat;
	import flash.display.Sprite;
	import flash.events.Event;
	public class Debug 
	{
		private var label:TextField;
		public var labelText:String = "Hello world and welcome to the show.";
		public function Debug() 
		{
			configureLabel();
            setLabel(labelText);
			
		}
		private function configureLabel():void {
			
            label = new TextField();
            label.autoSize = TextFieldAutoSize.LEFT;
            label.background = true;
            label.border = true;

            var format:TextFormat = new TextFormat();
            format.font = "Verdana";
            format.color = 0xFF0000;
            format.size = 10;
            format.underline = true;

            label.defaultTextFormat = format;
            
        }
		public function clearLabel():void {
			label.text ="";
		}
		
		public function setLabel(str:String):void {
            label.text +=str;
        }
		public function getLabel():TextField {
			
			return label;
		}
		
		
		
		
	}

}