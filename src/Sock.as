package  
{
	/**
	 * ...
	 * @author q
	 */
	import flash.net.XMLSocket;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.DataEvent;
	public class Sock 
	{
		public static var sock:XMLSocket;   
		public function Sock() 
		{
			doConnect();
		}
		private  function doConnect():void  
		{  
			
			sock = new XMLSocket("127.0.0.1", 9001);    
			sock.addEventListener(Event.CONNECT, onConnect);  
			sock.addEventListener(IOErrorEvent.IO_ERROR, onError);  
		}  
		private function onConnect(evt:Event):void  
		{  
			trace("Connected");  
			sock.removeEventListener(Event.CONNECT, onConnect);  
			sock.removeEventListener(IOErrorEvent.IO_ERROR, onError);  
		  
			sock.addEventListener(DataEvent.DATA, onDataReceived);  
			sock.addEventListener(Event.CLOSE, onSocketClose);              
			 
		}  
		
		public function send(txt:String):void {
			
			 sock.send(txt);
			 trace("Send="+txt);
			
		}
      
		private function onError(evt:IOErrorEvent):void  
		{  
			trace("Connect failed");  
			sock.removeEventListener(Event.CONNECT, onConnect);  
			sock.removeEventListener(IOErrorEvent.IO_ERROR, onError);  
			  
		}
		
		private function onDataReceived(evt:DataEvent):void  
		{  trace('onDataReceived');
			try {  
				trace("From Server:",  evt.data );  
			}  
			catch (e:Error) {  
				trace('error');  
			}  
		}  
  
		private function onSocketClose(evt:Event):void  
		{  
			trace("Connection Closed");  
			//stage.removeEventListener(KeyboardEvent.KEY_UP, keyUp);  
			sock.removeEventListener(Event.CLOSE, onSocketClose);  
			sock.removeEventListener(DataEvent.DATA, onDataReceived);  
		}
	}

}